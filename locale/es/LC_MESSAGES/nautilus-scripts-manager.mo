��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �  Z  �  !   =	  %   _	     �	  -   �	     �	     �	  :   �	     
     
  �  3
  ,   �  *   ,  	   W     a       z   �               4  '   M  >   u  F   �     �  Y                	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2012-08-26 15:45+0200
Last-Translator: David Hernando <david.hernando.m@gmail.comS>
Language-Team: es-ES <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %(name)s, enlazado como %(links)s %s no es un enlace, no puedo borrarlo Activo Todos los scripts deben ser instalados en %s. Error Error: No hay interfaz gráfica, por favor seleccione un comando. Enlace %s borrado. Gestor de scripts de Nautilus El gerente de scripts de Nautilus es software libre, puede redistribuirlo y / o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la Free Software Foundation, versión 3.

El gerente de scripts de Nautilus se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía implícita de COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública General de GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General GNU junto con el gestor de scripts de Nautilus, y si no, escriba a la Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, EE.UU.. Pagina web del gestor de scripts de Nautilus Por favor, seleccione al menos un comando. Posición La posición %s está ocupada Script Script %(script_name)s esta ya enlazado desde %(link)s (usa el argumento -p para añadir un enlace a una nueva posición). Script %s no esta activado! Script %s desconocido. Script %s no encontrado. La ruta %s ya existe (y no es un link)! La posición %(new_pos)s esta ocupada por el script %(owner)s. Para que los cambios tengan efecto, debes cerrar y reiniciar Nautilus. Advertencia: el objeto %(link_target)s del enlace %(link_path)s no se encuentra en %(scripts_folder)s. 