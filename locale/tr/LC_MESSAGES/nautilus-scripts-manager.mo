��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �  �  �  &   �	  %   �	     �	  *   �	     
     $
  7   *
     b
     |
  �  �
  %   ]     �     �     �     �  f   �     /     O     h  -   �  @   �  ^   �     O  p   W             	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2011-09-27 16:02+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish (http://www.transifex.net/projects/p/nautilus-scripts-manager/team/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=1; plural=0
 %(name)s, %(links)s olarak bağlandı. %s bir bağlantı değil. Silinemiyor Etkin Bütün betikler %s içine kurulmalıdır. Hata Hata: Grafik arayüz mevcut değil, lütfen bir komut seçin. %s bağlantısı silindi. Nautilus betik yönetici Nautilus betik yöneticisi özgür bir yazılımdır, onu Özgür Yazılım Vakfı'nın yayınladığı GNU Genel Kamu Lisansı'nın 3. sürümü veya (tercihinize bağlı) daha sonraki sürümleri altında dağıtabilir ve/veya değiştirebilirsiniz.

Nautilus betik yöneticisi faydalı olacağı umut edilerek dağıtılmaktadır, fakat HİÇBİR GARANTİSİ YOKTUR; hatta ÜRÜN DEĞERİ ya da BİR AMACA UYGUNLUK gibi garantiler de vermez. Lütfen GNU Genel Kamu Lisansı'nı daha fazla ayrıntı için inceleyin.

GNU Genel Kamu Lisansı'nı Nautilus ile birlikte almış olmalısınız, eğer almadıysanız lütfen Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus betik yönetici web sayfası Lütfen bir komut seçin. Konum %s konumu zaten kullanılıyor Betik %(script_name)s %(link)s olarak bağlanmıştı (yeni komuna bağlamak için -p argümanı kullanın). %s betiği etkinleştirilemedi. %s betiği tanınmıyor. %s betiği bulunamıyor. %s konumu zaten mevcut(ve bağlantı değil)! %(new_pos)s konumu %(owner)s betiği tarafından kullanılıyor. Yapılan değişikliklerin geçerli olması için Nautilus'u tekrar başlatmanız gerekebilir. Uyarı: %(link_path)s bağlantısının hedefi olan %(link_target)s kayıp veya %(scripts_folder)s dizininin dışında. 