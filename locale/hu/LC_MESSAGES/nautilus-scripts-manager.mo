��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �  �  �  #   �	  .   �	     �	  )   
     .
     3
  =   :
     x
     �
  �  �
  %   �  $   �     �     �       t   !     �     �     �  :   �  @      ?   a     �  k   �             	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2011-09-27 16:02+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Hungarian (http://www.transifex.net/projects/p/nautilus-scripts-manager/team/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1)
 %(name)s, kapcsolat, mint %(links)s %s nem egy kapcsolat, nem lehet eltávolítani Aktív Minden szkript telepítve kell legyen %s. Hiba Hiba:  Grafikus felület nem használható, válasszon egy parancsot Kapcsolat %s eltávolítva Nautilus szkript menedzser A Nautilus szkript menedzser szabad szoftver, terjesztheti és/vagy módosíthatja a Free Software Foundation által kiadott GNU General Public License harmadik (vagy bármely késÅbbi) változatában foglaltak alapján.

A Nautilus szkript menedzser programot abban a reményben terjesztjük, hogy hasznos lesz, de nem vállalunk SEMMIFÉLE GARANCIÁT, még olyan értelemben sem, hogy a program alkalmas-e a KÖZREADÁSRA vagy EGY BIZONYOS FELADAT ELVÉGZÉSÉRE. További részletekért tanulmányozza a GNU GPL licencet. A Nautilus szkript menedzser.

A programhoz a GNU General Public License egy példánya is jár, ha nem kapta meg, írjon a Free Software Foundation Inc.-nek. Levélcímük: Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus szkript menedzser web oldala Legfeljebb egy parancsot válasszon. Helyzet A helyzet %s már foglalt. Szkript Szkript %(script_name)s már létezÅ kapcsolat innen: %(link)s (use argument -p to add a link in a new position). Szkript %s nem megengedett! Szkript %s ismeretlen. Szkript %s nem található. Az elérési út %s már létezik (és nem egy kapcsolat)! A helyzet %(new_pos)s már használt a szkript %(owner)s által. A változások életbe lépéséhez indítsa újra a Nautilust. Figyelmeztetés:  a célkapcsolat %(link_target)s útvonala %(link_path)s elveszett vagy nem tartozik ide %(scripts_folder)s. 